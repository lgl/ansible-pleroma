#jinja2: lstrip_blocks: "true"
# Pleroma instance configuration

# https://git.pleroma.social/pleroma/pleroma/blob/develop/config/config.exs
# https://git.pleroma.social/pleroma/pleroma/-/blob/develop/docs/configuration/cheatsheet.md

# This file is based on:
# /opt/pleroma-pleroma-2.4.0-516-gdc63aaf8/release/bin/pleroma_ctl instance gen
#   --output config.exs
#   --output-psql out.sql # this file create the database and the enable some PostgreSQL extensions
#   --domain social.example.com
#   --instance-name "Social@example.com"
#   --admin-email admin@example.com
#   --notify-email notify@example.com
#   --db-configurable n
#   --dbhost db.example.com
#   --dbname pleroma
#   --dbuser pleroma
#   --dbpass password
#   --rum y
#   --uploads-dir /var/lib/pleroma/uploads
#   --static-dir /var/lib/pleroma/static
#   --listen-ip 192.168.0.1
#   --listen-port 4000
#   --indexable n
#   --anonymize-uploads n
#   --dedupe-uploads n

import Config

config :pleroma, Pleroma.Web.Endpoint,
   url: [host: "{{ pleroma_host }}", scheme: "https", port: 443],
   http: [ip: {127, 0, 0, 1}, port: {{ pleroma_port }}],
   secret_key_base: "{{ pleroma_secret_key_base }}",
   signing_salt: "{{ pleroma_signing_salt }}"

# Configure web push notifications
config :web_push_encryption, :vapid_details,
  subject: "mailto:{{ pleroma_admin_email }}",
  public_key: "{{ pleroma_web_push_encryption_public_key }}",
  private_key: "{{ pleroma_web_push_encryption_private_key }}"

{% if pleroma_smtp|default %}
# can be tested with /opt/pleroma/pleroma/bin/pleroma_ctl email test --to test@example.com
config :pleroma, Pleroma.Emails.Mailer,
  adapter: Swoosh.Adapters.SMTP,
  {% for k,v in pleroma_smtp.items() %}
  {{ k }}: {{ v }},
  {% endfor %}
  enabled: true
{% endif %}

config :pleroma, :instance,
  name: "{{ pleroma_instance }}",
  email: "{{ pleroma_admin_email }}",
  notify_email: "{{ pleroma_notify_email }}",
  limit: 5000,
  registrations_open: false

config :pleroma, :media_proxy,
  enabled: false,
  redirect_on_failure: true
  #base_url: "https://cache.pleroma.social"

config :pleroma, :database, rum_enabled: true
config :pleroma, :instance, static_dir: "/var/lib/{{ pleroma_user }}/static"
config :pleroma, Pleroma.Uploaders.Local, uploads: "/var/lib/{{ pleroma_user }}/uploads"

# Enable Strict-Transport-Security once SSL is working:
# config :pleroma, :http_security,
#   sts: true

# Configure S3 support if desired.
# The public S3 endpoint (base_url) is different depending on region and provider,
# consult your S3 provider's documentation for details on what to use.
#
# config :pleroma, Pleroma.Upload,
#  uploader: Pleroma.Uploaders.S3,
#  base_url: "https://s3.amazonaws.com"
#
# config :pleroma, Pleroma.Uploaders.S3,
#   bucket: "some-bucket",
#   bucket_namespace: "my-namespace",
#   truncated_namespace: nil,
#   streaming_enabled: true
#
# Configure S3 credentials:
# config :ex_aws, :s3,
#   access_key_id: "xxxxxxxxxxxxx",
#   secret_access_key: "yyyyyyyyyyyy",
#   region: "us-east-1",
#   scheme: "https://"
#
# For using third-party S3 clones like wasabi, also do:
# config :ex_aws, :s3,
#   host: "s3.wasabisys.com"

config :pleroma, configurable_from_database: false

config :pleroma, Pleroma.Upload, filters: [Pleroma.Upload.Filter.Exiftool]

config :joken, default_signer: "{{ pleroma_joken_default_signer }}"

import_config "db.exs"
