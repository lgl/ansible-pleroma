import Config

# config :pleroma, :i_am_aware_this_may_cause_data_loss,
#  disable_migration_check: true

config :pleroma, Pleroma.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "{{ pleroma_db_user }}",
  password: "{{ pleroma_db_password.replace("\\", "\\\\").replace("\"", "\\\"") }}",
  database: "{{ pleroma_db_name }}",
  hostname: "{{ pleroma_db_host }}"
